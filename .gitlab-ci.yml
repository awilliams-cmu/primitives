.ci_template: &ci_template
  stage: test

  before_script:
    # We have to move socket to a different location because we also get socket mounted
    # from the host. But we do not want to use that one but have Docker-in-Docker.
    - |
      mkdir -p /etc/docker
      echo '{"storage-driver": "overlay2", "hosts": ["unix:///var/run/docker2.sock"]}' > /etc/docker/daemon.json
    - start-stop-daemon --start --background --no-close --exec /usr/bin/dockerd >> /var/log/docker.log 2>&1
    - sleep 5
    - export DOCKER_HOST=unix:///var/run/docker2.sock
    - docker info || ( cat /var/log/docker.log && false )
    - free -h
    - nvidia-container-cli --debug=/dev/stdout info || true
    - ls -al /proc/driver/nvidia/gpus/ || true
    - git -C /data/datasets show -s
    - git -C /data/datasets_public show -s
    - ls -al /data/static_files
    - docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
    - mkdir results

  variables:
    GIT_STRATEGY: clone
    GIT_SUBMODULE_STRATEGY: recursive
    # Make sure Python's logging output is colored.
    TERM: ANSI

  artifacts:
    name: "results-$CI_JOB_ID"
    when: always
    paths:
      - results

variables:
  DOCKER_HOST: tcp://docker:2375
  GIT_STRATEGY: clone
  GIT_SUBMODULE_STRATEGY: recursive

services:
  # We use our copy of docker:19-dind Docker image to not hit Docker Hub pull limits.
  - name: registry.gitlab.com/datadrivendiscovery/images/docker:19-dind
    alias: docker
    variables:
      DOCKER_DRIVER: overlay2
      DOCKER_TLS_CERTDIR: ""

before_script:
  - docker info

validate_unique_id:
  stage: test

  image: registry.gitlab.com/datadrivendiscovery/images/testing-lite:$IMAGE_TAG

  before_script:
    - git lfs fetch --all

  script:
    - python3 ./run_unique_id_check.py primitives

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

run_unique_dependencies:
  stage: test

  image: registry.gitlab.com/datadrivendiscovery/images/testing-lite:$IMAGE_TAG

  before_script:
    - git lfs fetch --all

  script:
    - python3 ./run_unique_dependencies.py primitives

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

validate:
  stage: test

  image: registry.gitlab.com/datadrivendiscovery/images/testing-lite:$IMAGE_TAG

  before_script:
    - git lfs fetch --all

  script:
    - python3 ./run_validation.py --clean --image $IMAGE_TAG

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

  except:
    refs:
      - master@datadrivendiscovery/primitives

validate_devel:
  stage: test

  image: registry.gitlab.com/datadrivendiscovery/images/testing-lite:$IMAGE_TAG

  before_script:
    - git lfs fetch --all

  script:
    - python3 ./run_validation.py --devel --clean --image $IMAGE_TAG

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

  except:
    refs:
      - master@datadrivendiscovery/primitives

validate_all:
  stage: test

  image: registry.gitlab.com/datadrivendiscovery/images/testing-lite:$IMAGE_TAG

  parallel: 8

  before_script:
    - git lfs fetch --all

  script:
    - python3 ./run_validation.py --clean --all --parallel-index "$CI_NODE_INDEX" --parallel-total "$CI_NODE_TOTAL" --image $IMAGE_TAG

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

  only:
    refs:
      - master@datadrivendiscovery/primitives

validate_all_devel:
  stage: test

  image: registry.gitlab.com/datadrivendiscovery/images/testing-lite:$IMAGE_TAG

  parallel: 8

  before_script:
    - git lfs fetch --all

  script:
    - python3 ./run_validation.py --devel --clean --all --parallel-index "$CI_NODE_INDEX" --parallel-total "$CI_NODE_TOTAL" --image $IMAGE_TAG

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

  only:
    refs:
      - master@datadrivendiscovery/primitives

validate_coverage:
  stage: test

  image: registry.gitlab.com/datadrivendiscovery/images/testing-lite:$IMAGE_TAG

  before_script:
    - git lfs fetch --all

  script:
    - python3 ./run_coverage.py primitives

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

  only:
    refs:
      - master@datadrivendiscovery/primitives

  allow_failure: true

pipelines_ci_modified:
  <<: *ci_template

  stage: test

  image: registry.gitlab.com/datadrivendiscovery/images/testing:$IMAGE_TAG

  tags:
    - docker
    - datasets

  script:
    - ./ci.py --datasets /data/datasets --volumes /data/static_files --output results --modified_files_only primitives --image $IMAGE_TAG

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

  except:
    refs:
      - master@datadrivendiscovery/primitives

pipelines_ci_gpu_modified:
  <<: *ci_template

  stage: test

  image: registry.gitlab.com/datadrivendiscovery/images/testing:$IMAGE_TAG

  tags:
    - gpu
    - datasets

  script:
    - ./ci.py --datasets /data/datasets --volumes /data/static_files --output results --gpu --modified_files_only primitives --image $IMAGE_TAG

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

  except:
    refs:
      - master@datadrivendiscovery/primitives

  # Allowing gpu job to fail because public primitives repository does not have GPU CI runners available
  allow_failure: true

# Do not rename this job because name is used in dashboard to select jobs associated with CI runs.
pipelines_ci:
  <<: *ci_template

  image: registry.gitlab.com/datadrivendiscovery/images/testing:$IMAGE_TAG

  tags:
    - docker
    - datasets

  script:
    - ./ci.py --datasets /data/datasets --volumes /data/static_files --output results primitives --image $IMAGE_TAG

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

  only:
    refs:
      - master@datadrivendiscovery/primitives
    variables:
      - $RUN_PIPELINES_CI == 'true' && $REQUIRES_GPU != 'true'

  # Validating all pipelines is likely to uncover some failures
  allow_failure: true

# Do not rename this job because name is used in dashboard to select jobs associated with CI runs.
pipelines_ci_gpu:
  <<: *ci_template

  image: registry.gitlab.com/datadrivendiscovery/images/testing:$IMAGE_TAG

  tags:
    - gpu
    - datasets

  script:
    - ./ci.py --datasets /data/datasets --volumes /data/static_files --output results --gpu primitives --image $IMAGE_TAG

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

  only:
    refs:
      - master@datadrivendiscovery/primitives
    variables:
      - $RUN_PIPELINES_CI == 'true' && $REQUIRES_GPU == 'true'

  # Allowing gpu job to fail because public primitives repository does not have GPU CI runners available
  allow_failure: true

git_check:
  stage: build

  image: registry.gitlab.com/datadrivendiscovery/images/testing-docker:ubuntu-bionic

  script:
    - ./git-check.sh
    - git lfs fetch --all
    - ./git-check.sh

trigger_images_rebuild:
  stage: deploy

  image: registry.gitlab.com/datadrivendiscovery/images/testing-docker:ubuntu-bionic

  script:
    # This triggers a pipeline in https://gitlab.com/datadrivendiscovery/images
    - curl --fail -X POST -F token=$TRIGGER_TOKEN -F ref=master https://gitlab.com/api/v4/projects/5024100/trigger/pipeline

  only:
    refs:
      - master@datadrivendiscovery/primitives
    variables:
      - $TRIGGER_TOKEN

  needs: ["validate_all"]
